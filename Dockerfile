########
# BASE #
########

FROM node:21-alpine3.19 AS base

# Pnpm configuration
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable

WORKDIR /app

COPY package*.json pnpm-lock.yaml ./

RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --prod --frozen-lockfile

#########
# BUILD #
#########

FROM base AS build

RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --frozen-lockfile

COPY . .

RUN pnpm run build

##############
# PRODUCTION #
##############

FROM base AS production

RUN apk add --no-cache \
    curl=8.9.1-r1 \
    unzip=6.0-r14 \
    bind-tools=9.18.31-r0 \
    docker-cli=25.0.5-r1 \
    docker-cli-compose=2.23.3-r3 \
    bash=5.2.21-r0 \
    jq=1.7.1-r0

RUN ARCH=$(uname -m) && \
    if [ "$ARCH" = "x86_64" ]; then \
        wget https://github.com/mikefarah/yq/releases/download/v4.44.3/yq_linux_amd64 -O /usr/bin/yq; \
    elif [ "$ARCH" = "aarch64" ]; then \
        wget https://github.com/mikefarah/yq/releases/download/v4.44.3/yq_linux_arm64 -O /usr/bin/yq; \
    else \
        echo "Unsupported architecture: $ARCH" && exit 1; \
    fi && \
    chmod +x /usr/bin/yq

COPY --from=build /app/dist /app/dist
COPY --from=build /app/logs /app/logs

CMD ["node", "dist/index.js"]