import { type IConfig } from './configuration';
import axios, { type Method } from 'axios';

interface IApiOptions {
  method?: Method;
  data?: object;
}

export class HaoApi {
  constructor(private readonly config: IConfig) {}

  private async api(endpoint: string, options?: IApiOptions) {
    const method = options?.method || 'GET';
    const data = options?.data || {};
    return await axios({
      url: `${this.config.haoUrl}${endpoint}`,
      method,
      data,
      timeout: 120000,
    });
  }

  async getSerial() {
    return (await this.api('/misc/serial')).data.serial;
  }

  async postBlink(duration: number) {
    return (
      await this.api('/leds/blink', { method: 'POST', data: { duration } })
    ).data;
  }
}
