import { type Socket, io } from 'socket.io-client';
import { Logger } from './logger';
import { type DisconnectDescription } from 'socket.io-client/build/esm/socket';
import { HaoApi } from './hao-api';
import { type IConfig } from './configuration';
import { exec } from 'child_process';
import { DeployStatusEnum } from './enums/deploy-status.enum';
import { existsSync, mkdirSync, readFileSync, rmSync, writeFileSync } from 'fs';
import { IIdentity } from './interfaces/identity.interface';
import { getDeployId } from './utils/get-deploy-id';
import { sleep } from './utils/sleep';
import { getPublicIp } from './utils/get-public-ip';
import { getFqdn } from './utils/get-fqdn';
import { DeviceStatusEnum } from './enums/device-status.enum';
import { IOnDeployPayload } from './interfaces/on-deploy-payload.interface';
import { getVirtualMachineSaveId } from './utils/get-virtual-machine-save-id';
import { IOnCertificateVersionPayload } from './interfaces/on-certificate-version-payload.interface';
import { IOnCertificatePayload } from './interfaces/on-certificate-payload.interface';
import { join } from 'path';
import { createHash } from 'crypto';
import { execute } from './utils/execute';

export class WebSocketClient {
  private readonly config: IConfig;
  private readonly haoApi: HaoApi;
  private socket: Socket | undefined;
  private identity: IIdentity;
  private certificatesPath = '/olip-files/certificates';

  constructor(config: IConfig) {
    if (!existsSync(this.certificatesPath)) {
      mkdirSync(this.certificatesPath, { recursive: true });
    }
    this.config = config;
    this.haoApi = new HaoApi(config);
    this.identity = { status: this.getStatus(), deployId: getDeployId() };
    setInterval(
      this.emitCertificateVersion.bind(this),
      config.checkSslInterval,
    );
  }

  async start(): Promise<void> {
    Logger.log('Starting websocket client...');

    await this.fetchIdentityAndConnect();

    Logger.log('Websocket client successfully started !');
  }

  private registerListeners(unregister: boolean = false) {
    if (unregister) {
      Logger.log('Unregistering listeners...');
    } else {
      Logger.log('Registering listeners...');
    }
    this.registerListener('error', this.onError, unregister);
    this.registerListener(
      'certificate.version',
      this.onCertificateVersion,
      unregister,
    );
    this.registerListener('certificate', this.onCertificate, unregister);
    this.registerListener('connect', this.onConnect, unregister);
    this.registerListener('disconnect', this.onDisconnect, unregister);
    this.registerListener(
      `device.${this.identity.macAddress}.deploy`,
      this.onDeploy,
      unregister,
    );
    this.registerListener(
      `device.${this.identity.macAddress}.reset`,
      this.onReset,
      unregister,
    );
    this.registerListener(
      `device.${this.identity.macAddress}.blink`,
      this.onBlink,
      unregister,
    );
    this.registerListener(
      `device.${this.identity.macAddress}.exec.request`,
      this.onRequest,
      unregister,
    );
  }

  private updateIdentity() {
    this.identity = {
      ...this.identity,
      status: this.getStatus(),
      deployId: getDeployId(),
      virtualMachineSaveId: getVirtualMachineSaveId(),
    };
    this.connectSocket();
  }

  emitCertificateVersion() {
    if (!this.socket || this.socket.disconnected) return;
    this.emit('certificate.version');
  }

  emitCertificate() {
    this.emit('certificate');
  }

  emitDeploy({
    status,
    virtualMachineSaveId,
  }: {
    status: DeployStatusEnum;
    virtualMachineSaveId: string;
  }) {
    this.emit('device.deploy', { status, virtualMachineSaveId });
  }

  emitDeviceStatus(status: DeviceStatusEnum) {
    this.emit('device.status', { status });
  }

  emitDeployStatus(deployId: number, status: DeployStatusEnum) {
    this.socket?.emit('device.deploy.status', { deployId, status });
  }

  async onDeploy(payload: IOnDeployPayload) {
    Logger.log('received deploy order', payload);

    // Check if device is ready
    if (this.getStatus() !== DeviceStatusEnum.READY || !this.socket) {
      Logger.error('device not ready');
      return;
    }
    Logger.log('device is ready to be deployed');

    this.deploy(payload);
  }

  async onReset() {
    Logger.log('received reset order');
    try {
      await new Promise((resolve, reject) => {
        exec(`bash /deploy/scripts/reset.sh &`, (error, stdout, stderr) => {
          if (error) {
            console.error(error);
            reject(error || stderr);
          }
          console.log('stdout:', stdout);
          console.log('stderr:', stderr);
          resolve(stdout);
        });
      });
      this.socket?.emit(`device.reset`, {
        result: true,
      });
      this.updateIdentity();
      Logger.log('successfully reset device');
    } catch {
      this.socket?.emit(`device.reset`, {
        result: false,
      });
      Logger.error('failed to reset device');
    }
  }

  async onBlink(payload: { duration: number; id: number }) {
    Logger.log('received blink order');
    await this.haoApi.postBlink(payload.duration);
  }

  async onRequest(payload: { request: string }) {
    Logger.log('received request order');
    try {
      const result = await new Promise((resolve, reject) => {
        exec(payload.request, (error, stdout, stderr) => {
          if (error) {
            reject({ error, stderr });
          }
          resolve({ stdout, stderr });
        });
      });
      this.socket?.emit('device.exec.response', result);
    } catch (e) {
      Logger.error(e);
      this.socket?.emit('device.exec.response', e);
    }
  }

  async onConnect() {
    if (!this.socket) return;
    Logger.log('connected to websocket');
    this.emitCertificateVersion();
  }

  onDisconnect(reason: string, details: DisconnectDescription | undefined) {
    Logger.log('disconnected from websocket', { reason, details });
  }

  onError(error: unknown) {
    Logger.error(error);
  }

  private writeDeploy(payload: IOnDeployPayload) {
    if (!existsSync('/olip-files/deploy')) {
      mkdirSync('/olip-files/deploy', { recursive: true });
    }
    writeFileSync('/olip-files/deploy/id', `${payload.deployId}`);
  }

  private writeEnvironmentVariables(variables?: { [key: string]: string }) {
    if (!variables) return;

    Logger.log('writing environment variables...');
    for (const [key, value] of Object.entries(variables)) {
      Logger.log(`${key}=${value}`);
    }

    const envContent = Object.entries(variables)
      .map(([key, value]) => `${key}=${value}`)
      .join('\n');

    if (!existsSync('/deploy/envs')) {
      mkdirSync('/deploy/envs');
    }
    writeFileSync('/deploy/envs/.deploy.env', envContent);
  }

  private async deploy(payload: IOnDeployPayload) {
    this.writeAndEmitDeploying(payload);
    this.writeEnvironmentVariables(payload.variables);

    Logger.log('start deploy script...');
    try {
      if (!existsSync('/olip-files/deploy')) {
        mkdirSync('/olip-files/deploy');
      }
      const ssidPrefixPath = '/olip-files/deploy/ssid-prefix';
      const prefix = 'ideascube';
      writeFileSync(ssidPrefixPath, 'ideascube');

      if (!this.identity.macAddress || !this.identity.deployId) {
        Logger.error('identity.macAddress not found, could not deploy');
        return;
      }

      const ssid = `${prefix}-${payload.deployId.toString().slice(-4)}-${this.identity.macAddress.slice(-5).replace(':', '')}`;

      const dockerInspectResult = await execute(
        `docker inspect $(docker ps --filter "name=$(hostname)" --format "{{.Names}}") | jq -r '.[] | .Mounts[] | select(.Destination == "/deploy") | .Source'`,
      );
      if (!dockerInspectResult.stdout) {
        Logger.error('failed to deploy device');
        this.writeAndEmitError(payload);
        return;
      }

      const hostDeployPath = dockerInspectResult.stdout.trim();

      await execute(
        `bash /deploy/scripts/deploy.sh -v ${payload.virtualMachineSaveId} -b -s "${ssid}" -l ${payload.defaultLanguageIdentifier} --deploy-path ${hostDeployPath}`,
      );
      Logger.log('deploy script finished');
      this.writeAndEmitInstalling(payload);
    } catch (e) {
      Logger.error(e);
      this.writeAndEmitError(payload);
      Logger.error('failed to deploy device');
    }
  }

  async connectSocket() {
    if (this.socket) {
      Logger.log('Reconnecting to websocket after identity changed...');
      this.registerListeners(true);
      this.socket.disconnect();
    } else {
      Logger.log('Connecting to websocket...');
    }

    this.socket = io(this.config.websocketServerUrl, {
      auth: this.identity,
    });
    this.socket.connect();

    this.registerListeners();
  }

  private async emit(event: string, ...args: any[]): Promise<any> {
    if (!this.socket) {
      await new Promise<void>((resolve) => setTimeout(resolve, 1000));
      return this.emit(event, ...args);
    }
    Logger.log(`emit ${event} event`, ...args);
    this.socket.emit(event, ...args);
  }

  private getIpv4() {
    if (existsSync('/olip-files/network/lan')) {
      return readFileSync('/olip-files/network/lan').toString().trim();
    }
    return undefined;
  }

  private getMacAddress() {
    if (existsSync('/olip-files/network/mac-address')) {
      return readFileSync('/olip-files/network/mac-address').toString().trim();
    }
    return undefined;
  }

  private async fetchIdentityAndConnect(tries: number = 1): Promise<void> {
    Logger.log('Fetching identity...');
    let identitySuccessfull = true;

    // Get mac address
    const macAddress = this.getMacAddress();

    const privateIpv4 = this.getIpv4();

    // Get serial
    const serialId = this.config.serialId || (await this.haoApi.getSerial());

    // Get public IPs
    let publicIpv4 = undefined;
    let publicIpv6 = undefined;
    try {
      const publicIp = await getPublicIp();
      if (!publicIp || (!publicIp.ipv4 && !publicIp.ipv6)) {
        Logger.error('Public IP not found');
        identitySuccessfull = false;
      } else {
        publicIpv4 = publicIp.ipv4 || undefined;
        publicIpv6 = publicIp.ipv6 || undefined;
      }
    } catch (e) {
      Logger.error(e);
      identitySuccessfull = false;
    }

    // Get IpV4 FQDN
    let ipv4FqdnName = undefined;
    let ipv4FqdnArpa = undefined;
    if (publicIpv4) {
      try {
        const ipv4Fqdn = await getFqdn(publicIpv4);
        if (!ipv4Fqdn || !ipv4Fqdn.arpa || !ipv4Fqdn.name) {
          Logger.error('IpV4 FQDN not found');
          identitySuccessfull = false;
        } else {
          ipv4FqdnName = ipv4Fqdn.name || undefined;
          ipv4FqdnArpa = ipv4Fqdn.arpa || undefined;
        }
      } catch (e) {
        Logger.error(e);
        identitySuccessfull = false;
      }
    }

    // Get IpV6 FQDN
    let ipv6FqdnName = undefined;
    let ipv6FqdnArpa = undefined;
    if (publicIpv6) {
      try {
        const ipv6Fqdn = await getFqdn(publicIpv6);
        if (!ipv6Fqdn || !ipv6Fqdn.arpa || !ipv6Fqdn.name) {
          Logger.error('IpV6 FQDN not found');
          if (!publicIpv4 || (!ipv4FqdnName && !ipv4FqdnArpa)) {
            identitySuccessfull = false;
          }
        } else {
          ipv6FqdnName = ipv6Fqdn.name || undefined;
          ipv6FqdnArpa = ipv6Fqdn.arpa || undefined;
        }
      } catch (e) {
        Logger.error(e);
        if (!publicIpv4 || (!ipv4FqdnName && !ipv4FqdnArpa)) {
          identitySuccessfull = false;
        }
      }
    }

    if (!identitySuccessfull) {
      if (tries < this.config.maxIdentityTries) {
        Logger.warn(
          `Identity fetch failed, retrying in ${this.config.identityTriesInterval} seconds...`,
        );
        await sleep(this.config.identityTriesInterval * 1000);
        return this.fetchIdentityAndConnect(tries + 1);
      } else {
        Logger.warn(
          `Identity fetch failed, max retries reached, retrying in ${this.config.identityFailRetryInterval} seconds...`,
        );
        this.retryFetchIdentity();
      }
    }

    this.identity = {
      status: this.getStatus(),
      deployId: getDeployId(),
      virtualMachineSaveId: getVirtualMachineSaveId(),
      macAddress,
      serialId,
      privateIpv4,
      publicIpv4,
      ipv4FqdnName,
      ipv4FqdnArpa,
      publicIpv6,
      ipv6FqdnName,
      ipv6FqdnArpa,
    };

    Logger.log('Identity', this.identity);

    if (identitySuccessfull) {
      this.connectSocket();
    }

    if (!this.socket) {
      this.connectSocket();
    }
  }

  async retryFetchIdentity() {
    await sleep(this.config.identityFailRetryInterval * 1000);
    await this.fetchIdentityAndConnect();
  }

  private registerListener(
    listener: string,
    callback: (...args: any[]) => void | Promise<void>,
    unregister: boolean = false,
  ) {
    if (!this.socket) return;
    if (unregister) {
      Logger.log(`Unregistered listener "${listener}"`);
      this.socket.off(listener, callback.bind(this));
    } else {
      Logger.log(`Registered listener "${listener}"`);
      this.socket.on(listener, callback.bind(this));
    }
  }

  getStatus(): DeviceStatusEnum {
    const statusPath = '/olip-files/status';
    if (!existsSync(statusPath)) {
      return DeviceStatusEnum.READY;
    } else {
      return readFileSync(statusPath).toString().trim() as DeviceStatusEnum;
    }
  }

  async writeStatus(status: DeviceStatusEnum) {
    writeFileSync('/olip-files/status', `${status}`);
    this.updateIdentity();
  }

  private writeAndEmitDeploying(payload: IOnDeployPayload) {
    this.writeDeploy(payload);
    // Write new status
    this.writeStatus(DeviceStatusEnum.DEPLOYING);

    // Emit new status
    this.emitDeployStatus(payload.deployId, DeployStatusEnum.DEPLOYING);
  }

  private writeAndEmitInstalling(payload: IOnDeployPayload) {
    this.writeStatus(DeviceStatusEnum.INSTALLING);
    this.emitDeployStatus(payload.deployId, DeployStatusEnum.INSTALLING);
  }

  private writeAndEmitError(payload: IOnDeployPayload) {
    this.emitDeployStatus(payload.deployId, DeployStatusEnum.ERROR);
    this.writeStatus(DeviceStatusEnum.ERROR);
  }

  private onCertificateVersion(payload: IOnCertificateVersionPayload) {
    Logger.log('received certificate version', payload);
    const versionPath = `${this.certificatesPath}/version`;

    let version = null;
    if (existsSync(versionPath)) {
      version = readFileSync(versionPath).toString();
    }

    if (version !== payload.version || !this.isCertificateNeeded()) {
      if (!existsSync(join(this.certificatesPath, 'need-certificate')))
        writeFileSync(join(this.certificatesPath, 'need-certificate'), '');
      this.emitCertificate();
    }

    writeFileSync(versionPath, payload.version);
  }

  private isCertificateOk(): boolean {
    if (!existsSync(join(this.certificatesPath, 'version'))) return false;
    if (!existsSync(join(this.certificatesPath, '_.ideascube.io.crt')))
      return false;
    const version = readFileSync(
      join(this.certificatesPath, 'version'),
    ).toString();
    const certificate = readFileSync(
      join(this.certificatesPath, '_.ideascube.io.crt'),
    ).toString('base64');
    const md5 = createHash('md5').update(certificate).digest('hex');
    if (md5 !== version) return false;
    return true;
  }

  private isCertificateNeeded() {
    if (existsSync(join(this.certificatesPath, 'need-certificate')))
      return false;
    return this.isCertificateOk();
  }

  private onCertificate(payload: IOnCertificatePayload) {
    Logger.log('received certificate', payload);

    writeFileSync(
      join(this.certificatesPath, '_.ideascube.io.crt'),
      Buffer.from(payload.certificate, 'base64').toString('utf8'),
    );

    writeFileSync(
      join(this.certificatesPath, 'ideascube.io.key'),
      Buffer.from(payload.key, 'base64').toString('utf8'),
    );

    if (
      existsSync(join(this.certificatesPath, 'need-certificate')) &&
      this.isCertificateOk()
    ) {
      rmSync(join(this.certificatesPath, 'need-certificate'));
    }
  }
}
