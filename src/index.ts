import { Config } from './configuration';
import { Logger } from './logger';
import { WebsocketApi } from './websocket-api';
import { WebSocketClient } from './websocket-client';

async function bootstrap() {
  Logger.log('Loading configuration...');
  const { config } = new Config();
  const webSocketClient = new WebSocketClient(config);
  webSocketClient.start();
  new WebsocketApi(config, webSocketClient);
}

bootstrap();
