import { IContext } from './context.interface';

export interface IOnDeployPayload {
  virtualMachineSaveId: number;
  deployId: number;
  defaultLanguageIdentifier: string;
  context: IContext;
  variables?: {
    [key: string]: string;
  };
}
