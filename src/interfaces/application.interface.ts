export interface IApplication {
  id: string;
  name: string;
  displayName: string;
  port: string;
  size: number;
  version: string;
  logo: string;
  image: string;
  compose: string;
  hooks: string;
  md5: string;
  catalogFunction: string | null;
}
