export interface IOnCertificatePayload {
  certificate: string;
  key: string;
}
