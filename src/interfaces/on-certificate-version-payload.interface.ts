export interface IOnCertificateVersionPayload {
  version: string;
}
