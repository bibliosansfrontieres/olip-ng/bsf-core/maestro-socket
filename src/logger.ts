import { format, createLogger, transports } from 'winston';
import { consoleFormat } from 'winston-console-format';

export class Logger {
  private static readonly logger = createLogger({
    format: format.combine(
      format.timestamp(),
      format.ms(),
      format.errors({ stack: true }),
      format.splat(),
      format.json(),
    ),
    transports: [
      new transports.Console({
        format: format.combine(
          format.colorize({ all: true }),
          format.padLevels(),
          consoleFormat({
            showMeta: true,
            inspectOptions: {
              depth: Infinity,
              colors: true,
              maxArrayLength: Infinity,
              breakLength: 120,
              compact: Infinity,
            },
          }),
        ),
      }),
      new transports.File({
        filename: 'logs/websocket-client.log',
      }),
    ],
  });

  static log(message: string, ...args: any) {
    this.logger.log({ level: 'info', message, ...args });
  }
  static warn(message: string, ...args: any) {
    this.logger.warn({ level: 'warn', message, ...args });
  }

  static error(error: unknown) {
    this.logger.error(error);
  }
}
