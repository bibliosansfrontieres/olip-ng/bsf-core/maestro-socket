import express, { Express, Request, Response } from 'express';
import bodyParser from 'body-parser';
import { IConfig } from './configuration';
import { WebSocketClient } from './websocket-client';
import { DeployStatusEnum } from './enums/deploy-status.enum';
import { DeviceStatusEnum } from './enums/device-status.enum';

export class WebsocketApi {
  private app: Express;
  private websocketClient: WebSocketClient;

  constructor(config: IConfig, websocketClient: WebSocketClient) {
    this.websocketClient = websocketClient;
    this.app = express();
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());
    this.registerRoutes();
    this.app.listen(config.port);
  }

  registerRoutes() {
    this.app.post('/deploy', (req: Request, res: Response) => {
      if (!req.body) return res.sendStatus(401);

      const { status, virtualMachineSaveId } = req.body;

      if (!status || !Object.values(DeployStatusEnum).includes(status))
        return res.status(401).send('no status or invalid status sent');
      if (!virtualMachineSaveId)
        return res.status(401).send('no virtualMachineSaveId sent');

      this.websocketClient.emitDeploy({ status, virtualMachineSaveId });
      if (status === DeployStatusEnum.DEPLOYED) {
        this.websocketClient.writeStatus(DeviceStatusEnum.DEPLOYED);
      }
      res.sendStatus(200);
    });
  }
}
