import { existsSync, readFileSync } from 'fs';

export function getVirtualMachineSaveId(): string | undefined {
  let virtualMachineSaveId = undefined;

  const virtualMachineSaveIdPath = '/olip-files/virtual-machine-save-id';
  if (existsSync(virtualMachineSaveIdPath)) {
    virtualMachineSaveId = JSON.parse(
      readFileSync(virtualMachineSaveIdPath).toString(),
    ).virtualMachineSaveId;
  }

  return virtualMachineSaveId;
}
