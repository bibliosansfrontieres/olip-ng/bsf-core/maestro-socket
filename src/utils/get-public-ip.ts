import { exec } from 'child_process';
import { Logger } from '../logger';

async function dig(digCommand: string) {
  return await new Promise<string>((resolve, reject) => {
    exec(digCommand, (error, stdout, stderr) => {
      if (error) {
        Logger.error(error);
        reject(undefined);
      }
      if (stderr) {
        Logger.error(stderr);
        reject(undefined);
      }
      resolve(stdout);
    });
  });
}

function formatResult(toFormat: string) {
  let ipv4;
  let ipv6;
  if (toFormat.includes('failed')) return { ipv6, ipv4 };
  if (toFormat.includes(':')) {
    ipv6 = formatIp(toFormat);
  } else if (toFormat.includes('.')) {
    ipv4 = formatIp(toFormat);
  }
  return { ipv6, ipv4 };
}

function formatIp(toFormat: string) {
  return toFormat
    .trim()
    .replace(/"/g, '')
    .replace(/\n/g, '')
    .replace(/\s+/g, ' ');
}

export async function getPublicIp() {
  let ipv4;
  let ipv6;

  try {
    const googleV4DigResult = await dig(
      'dig -4 TXT +short o-o.myaddr.l.google.com @ns1.google.com',
    );
    if (googleV4DigResult) {
      const googleV4Dig = formatResult(googleV4DigResult);
      if (googleV4Dig.ipv4) ipv4 = googleV4Dig.ipv4;
    }
  } catch {
    Logger.error('Failed to get public ip from -4 google.com');
  }

  try {
    const googleV6DigResult = await dig(
      'dig -6 TXT +short o-o.myaddr.l.google.com @ns1.google.com',
    );
    if (googleV6DigResult) {
      const googleV6Dig = formatResult(googleV6DigResult);
      if (googleV6Dig.ipv6) ipv6 = googleV6Dig.ipv6;
    }
  } catch {
    Logger.error('Failed to get public ip from -6 google.com');
  }

  if (ipv6 && ipv4) {
    return { ipv6, ipv4 };
  }

  const opendnsDigResult = await dig(
    'dig +short myip.opendns.com @resolver1.opendns.com',
  );
  if (opendnsDigResult) {
    const opendnsDig = formatResult(opendnsDigResult);
    if (opendnsDig.ipv4) ipv4 = opendnsDig.ipv4;
    if (opendnsDig.ipv6) ipv6 = opendnsDig.ipv6;
  }

  if (ipv6 && ipv4) {
    return { ipv6, ipv4 };
  }

  const cloudflareDigResult = await dig(
    'dig +short txt ch whoami.cloudflare @1.0.0.1',
  );
  if (cloudflareDigResult) {
    const cloudflareDig = formatResult(cloudflareDigResult);
    if (cloudflareDig.ipv4) ipv4 = cloudflareDig.ipv4;
    if (cloudflareDig.ipv6) ipv6 = cloudflareDig.ipv6;
  }

  return { ipv6, ipv4 };
}
