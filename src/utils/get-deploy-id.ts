import { existsSync, readFileSync } from 'fs';

export function getDeployId(): string | undefined {
  let deployId = undefined;

  const deployPath = '/olip-files/deploy/id';
  if (existsSync(deployPath)) {
    deployId = readFileSync(deployPath).toString();
  }

  return deployId;
}
