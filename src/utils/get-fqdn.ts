import { exec } from 'child_process';
import { Logger } from '../logger';

export async function getFqdn(publicIp: string | undefined) {
  let arpa;
  let name;
  if (!publicIp) return { arpa, name };

  const nslookupResult = await new Promise<string>((resolve, reject) => {
    try {
      exec(`nslookup ${publicIp}`, (error, stdout, stderr) => {
        if (error) {
          Logger.error(error);
          reject(undefined);
        }
        if (stderr) {
          Logger.error(stderr);
          reject(undefined);
        }
        resolve(stdout);
      });
    } catch (e) {
      Logger.error(e);
      reject(undefined);
    }
  });

  if (!nslookupResult) return { arpa, name };

  const nslookupResultLines = nslookupResult.split('\n');

  const line = nslookupResultLines[0];
  const parts = line.split('name =');

  if (parts.length === 1) {
    if (parts[0].includes('arpa')) {
      arpa = parts[0].trim();
    } else if (parts[0].includes('.')) {
      name = parts[0].trim();
    }
    return { arpa, name };
  }

  if (parts.length > 1) {
    arpa = parts[0].trim();
    name = parts[1].trim();
    return { arpa, name };
  }

  return { arpa, name };
}
