import { spawn } from 'child_process';
import { Logger } from '../logger';

export type ExecuteResponse = {
  stdout: string;
  stderr: string;
  error?: unknown;
};

export async function execute(command: string): Promise<ExecuteResponse> {
  const id = Date.now();

  Logger.log(`[${id}] Executing command: ${command}`);

  let stdout = '';
  let stderr = '';
  let error = undefined;
  try {
    await new Promise<void>((resolve, reject) => {
      const spawnCommand = spawn('bash', ['-c', command]);

      spawnCommand.stdout.on('data', (data) => {
        stdout += data;
        Logger.log(`[${id}] stdout: ${data}`);
      });

      spawnCommand.stderr.on('data', (data) => {
        stderr += data;
        Logger.log(`[${id}] stderr: ${data}`);
      });

      spawnCommand.on('close', (code) => {
        if (!code) {
          Logger.log(`[${id}] command exited without exit code`);
        } else {
          Logger.log(`[${id}] command exited with exit code ${code}`);
        }

        resolve();
      });

      spawnCommand.on('error', (error) => {
        Logger.error(`[${id}] error: ${error}`);
        reject(error);
      });
    });
  } catch (e) {
    error = e;
  }

  return { stdout, stderr, error };
}
