import dotenv from 'dotenv';
import ms from 'ms';

export interface IConfig {
  identityTriesInterval: number;
  identityFailRetryInterval: number;
  maxIdentityTries: number;
  websocketServerUrl: string;
  haoUrl: string;
  interfaceName?: string;
  serialId?: string;
  port: number;
  checkSslInterval: number;
}

export class Config {
  public config: IConfig;

  constructor() {
    dotenv.config();
    this.config = this.load();
  }

  private load() {
    if (!process.env.WEBSOCKET_SERVER_URL) {
      throw new Error('WEBSOCKET_SERVER_URL is not defined');
    }
    if (!process.env.HAO_URL) {
      throw new Error('HAO_URL is not defined');
    }
    return {
      identityTriesInterval: Number(
        process.env.IDENTITY_TRIES_INTERVAL || '10',
      ),
      maxIdentityTries: Number(process.env.MAX_IDENTITY_TRIES || '5'),
      identityFailRetryInterval: Number(
        process.env.IDENTITY_FAIL_RETRY_INTERVAL || '60',
      ),
      websocketServerUrl: process.env.WEBSOCKET_SERVER_URL,
      haoUrl: process.env.HAO_URL,
      interfaceName: process.env.INTERFACE_NAME,
      serialId: process.env.MISC_SERIAL_ID,
      port: process.env.PORT ? Number(process.env.PORT) : 80,
      checkSslInterval: ms(process.env.CHECK_SSL_INTERVAL || '24h'),
    };
  }
}
